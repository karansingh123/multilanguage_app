import Blog from '@/containers/blog'
import React from 'react'
import { Container } from 'react-bootstrap'

const index = () => {
  return (
   <Container>
       <Blog/>
   </Container>
   
  )
}

export default index
