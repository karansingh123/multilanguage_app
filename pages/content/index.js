import Content from '@/containers/content'
import React from 'react'
import { Container } from 'react-bootstrap'

const index = () => {
  return (
  <Container>
    <Content/>
  </Container>
  )
}

export default index
