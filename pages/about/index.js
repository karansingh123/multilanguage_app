import About from '@/containers/about'
import React from 'react'

const index = () => {
  return (
    <div>
      <About/>
    </div>
  )
}

export default index
