
// import LanguaeState from '@/components/context/language/LanguageState';
import store from '@/components/redux/store';
import Header from '@/containers';
import '@/styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';
// import CustomAlert from '../src/containers/alert';
// import { useState } from 'react';
import { Provider } from 'react-redux';

export default function App({ Component, pageProps }) {
  // const [alert, setAlert] = useState(null);

  // const showAlert = (message, type) => {
  //   setAlert({
  //     msg: message,
  //     type: type
  //   })
  //   setTimeout(() => {
  //     setAlert(null);
  //   }, 3000);
  // }
  return (

    <>
      <Provider store={store}>
        <Header/>
        {/* <CustomAlert alert={alert}/> */}
        <Component {...pageProps} />
      </Provider>
    </>
    
  )
}
