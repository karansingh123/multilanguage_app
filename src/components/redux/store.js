
import { createStore } from 'redux';
// import rootReducer from './reducer/index';
import languageReducer from './reducer/languageReducer';

const store = createStore(languageReducer);
export default store;
