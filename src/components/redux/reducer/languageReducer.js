// import translations from '@/components/multilanguage';
// import { TOGGLE_MODE, CHANGE_LANGUAGE } from '../action/index';

// const initialState = {
//   mode: 'light',
//   language: 'en',
// };
// const languageReducer = (state = initialState, action) => {
//   switch (action.type) {
//     case TOGGLE_MODE:
//       return {
//         ...state,
//         mode: state.mode === 'light' ? 'dark' : 'light',
//         colors: {
//           ...state.colors,

//           background: state.mode === 'light' ? 'dark' : 'light',
//           text: state.mode === 'light' ? '#ffffff' : '#000000'
//         }

//       };
//     case CHANGE_LANGUAGE:
//       return {
//         ...state,
//         language: state.language === 'en' ? 'fn' : 'en',
//         Translations: translations[language],
//       };
//     default:
//       return state;
//   }

// };
// export default languageReducer;


import { TOGGLE_MODE, CHANGE_LANGUAGE } from '../action/index';

const initialState = {
  mode: 'light',
  language: 'en',
};

const toggleMode = (state) => {
  return {
    ...state,
     mode: state.mode === 'light' ? 'dark' : 'light',
  };
};

const changeLanguage = (state) => {
  return {
    ...state,
    language: state.language === 'en' ? 'fn' : 'en',
  };
  
  // console.log(language,'language')
};

const languageReducer = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_MODE:
      return toggleMode(state);
    case CHANGE_LANGUAGE:
      return changeLanguage(state);
    default:
      return state;
  }
};

export default languageReducer;
