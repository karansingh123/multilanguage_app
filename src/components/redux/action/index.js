export const TOGGLE_MODE = 'TOGGLE_MODE';
export const CHANGE_LANGUAGE = 'CHANGE_LANGUAGE';

export const toggleMode = (mode) => ({
  type: TOGGLE_MODE,
  payload:mode
});

export const changeLanguage = (language) => ({
  type: CHANGE_LANGUAGE,
  payload: language
});

