import en from './en/en';
import fn from './fn/fn';


const translations = {
  en,
  fn
};

export default translations;