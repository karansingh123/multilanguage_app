const fnTranslations = {
  AccordionItem1: "Il est très important pour le client de suivre le processus de coaching, mais je le fais à un tel moment, il tombe dans un grand travail et une grande douleur. Pour cela, je suis désolé de dire que personne ne devrait pratiquer n'importe quel type de travail, sauf que certains d'entre eux sont utiles. Ne vous précipitez pas dans la maison Il veut trouver à redire au plaisir, mais il n'y a pas besoin de fuir la douleur pour naître A moins qu'ils ne soient aveuglés par la luxure, ils n'en sortent pas, ils sont dans la culpabilité de ceux qui abandonnent leurs devoirs adoucit l'âme, c'est-à-dire les travaux.",
  AccordionItem2: "Vous pouvez dire qu'un livre, un film, etc. parle de quelque chose. De quoi parle l'histoire? Vous pouvez dire que quelque chose est, a ou a quelque chose à voir avec autre chose. Qu'est-ce que ça a à voir avec ça? La condition a à voir avec la génétique. C'est en grande partie lié à l'attitude.",
  Jumbotron: "À l'aide d'une série d'utilitaires, vous pouvez créer ce jumbotron, tout comme celui des versions précédentes de Bootstrap. Consultez les exemples ci-dessous pour savoir comment vous pouvez le remixer et le relooker à votre guise.",
  ChangeTheBackground: "Échangez l'utilitaire de couleur d'arrière-plan et ajoutez un utilitaire de couleur `.text-*` pour mélanger l'apparence du jumbotron. Ensuite, mélangez et assortissez avec des thèmes de composants supplémentaires et plus encore.",
  AddBorders: "Ou, gardez-le léger et ajoutez une bordure pour une définition supplémentaire des limites de votre contenu. Assurez-vous de regarder sous le capot le HTML source ici car nous avons ajusté l'alignement et la taille du contenu des deux colonnes pour une hauteur égale.",
  BlogTitle: "Titre d'un article de blog plus long",
  TitleContent: "Plusieurs lignes de texte qui forment le lede, informant rapidement et efficacement les nouveaux lecteurs sur ce qui est le plus intéressant dans le contenu de cet article.",
  CardOne: "Il s'agit d'une carte plus large avec un texte d'accompagnement ci-dessous comme introduction naturelle à du contenu supplémentaire",
  CardTwo: "Il s'agit d'une carte plus large avec un texte de support ci-dessous comme introduction naturelle à du contenu supplémentaire.",
  SimplePostBlog1: "Cet article de blog présente quelques types de contenu différents pris en charge et stylisés avec Bootstrap. La typographie de base, les listes, les tableaux, les images, le code, etc. sont tous pris en charge comme prévu.",
  SimplePostBlog2: "Il s'agit d'un contenu d'espace réservé de paragraphe supplémentaire. Il a été écrit pour remplir l'espace disponible et montrer comment un extrait de texte plus long affecte le contenu environnant. Nous le répéterons souvent pour que la démonstration continue, alors soyez à l'affût de cette même chaîne de texte.",
  Blockquote: "Il s'agit d'un contenu d'espace réservé de paragraphe supplémentaire. Il a été écrit pour remplir l'espace disponible et montrer comment un extrait de texte plus long affecte le contenu environnant. Nous le répéterons souvent pour que la démonstration continue, alors soyez à l'affût de cette même chaîne de texte.",
  BlockquoteExample: "Il s'agit d'un contenu d'espace réservé de paragraphe supplémentaire. C'est une version légèrement plus courte de l'autre corps de texte très répétitif utilisé partout. Voici un exemple de liste non ordonnée :",
  HtmlElement: "HTML définit une longue liste de balises en ligne disponibles, dont une liste complète peut être trouvée sur le",
  HtmlElementTag: "La plupart de ces éléments sont stylés par les navigateurs avec peu de modifications de notre part.",
  BlogHeading: "Il s'agit d'un contenu d'espace réservé de paragraphe supplémentaire. Il a été écrit pour remplir l'espace disponible et montrer comment un extrait de texte plus long affecte le contenu environnant. Nous le répéterons souvent pour que la démonstration continue, alors soyez à l'affût de cette même chaîne de texte.",
  BlogPost: "Il s'agit d'un contenu d'espace réservé de paragraphe supplémentaire. Il a été écrit pour remplir l'espace disponible et montrer comment un extrait de texte plus long affecte le contenu environnant. Nous le répéterons souvent pour que la démonstration continue, alors soyez à l'affût de cette même chaîne de texte.",
  HomeCarousel:"Il n'y a pas de vie libre, parfois molle avec un frémissement de propagande",
  MainHeading:"Conçu pour les ingénieurs",
  HeadingDetail:"Créez tout ce que vous voulez avec Aperture",
  Anotherheadline:"Un autre titre",
  Anotherheadlinedetail:"Et un sous-titre encore plus spirituel.",
  BlogAbout:"Personnalisez cette section pour informer un peu vos visiteurs de votre publication, de vos auteurs, de votre contenu ou de tout autre chose. Totalement à vous.",
  Anotherblogpost:"Une citation plus longue va ici, peut-être avec certains",
  inthemiddleofit:"au milieu de celui-ci.",
  Featureswithtitle:"Fonctionnalités avec titre",
  Featurespage:"Titre aligné à gauche expliquant ces fonctionnalités impressionnantes",
  FeaturespageParagraph:"Paragraphe de texte sous le titre pour expliquer le titre. Nous y ajouterons une autre phrase et continuerons probablement jusqu'à ce que nous manquions de mots.",
  Featuredtitle:"Titre en vedette",
  TitleParagrapg:"Paragraphe de texte sous le titre pour expliquer le titre.",
  ImgText1:"Titre court, veste longue",
  ImgText2:"Titre beaucoup plus long qui s'étend sur plusieurs lignes",
  ImgText3:"Un autre titre plus long appartient ici",
  Changethebackground:"Changer l'arrière-plan",
  Addborders:"Ajouter des bordures",
  Examplebutton:"Exemple de bouton",
  Primarybutton:"Bouton principal",
  Continuereading:"Continuer la lecture...",
  FromtheFirehose:"Du tuyau d'incendie",
  Sampleblogpost:"Exemple d'article de blog",
  Exampleblockquote:"Voici un exemple de blockquote en action :",
  Quotedtext:"Le texte cité va ici.",
  Longerblog:"Titre de l'article de blog plus long : celui-ci comporte plusieurs lignes !",
  AnotherBlogPost:"Ceci est un autre titre d'article de blog",
  Exampleblogpost:"Exemple de titre d'article de blog",
  Anotherblogpost2:"Un autre article de blog"



};

export default fnTranslations;