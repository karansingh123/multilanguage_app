import React, { useContext } from 'react'
import styles from '../container.module.css'
import Carousel from 'react-bootstrap/Carousel';
// import languageContext from '@/components/context/language/languageContext';
import { useDispatch, useSelector } from 'react-redux';
import translations from '@/components/multilanguage';

const HomePage = () => {
	// const { Translations, onChange, toggleMode, mode, alert } = useContext(languageContext);
	const dispatch = useDispatch();
	const language = useSelector(state => state.language);
	const Translations = translations[language];
	const mode = useSelector(state => state.mode);
  
	const containerBg = {
	  backgroundColor: mode === "dark" ? "black" : "white",
	}
	const containerColor = { color: mode === "dark" ? "white" : "black", }	
	return (
		<>
			<div  className=''>

				<div className=' pt-2'>
					<Carousel>
						<Carousel.Item>
							<img src="CarouselImage/Main Banner 01" height="450px" />
							<Carousel.Caption style={containerColor}>
								<h3 >First slide label</h3>
								<p >{Translations.HomeCarousel}</p>
							</Carousel.Caption>
						</Carousel.Item>
						<Carousel.Item>
							<img src="CarouselImage/Main Banner 02" height="450px" />
							<Carousel.Caption style={containerColor}>
								<h3 >Second slide label</h3>
								<p>{Translations.HomeCarousel}</p>
							</Carousel.Caption>
						</Carousel.Item>
						<Carousel.Item>
							<img src="CarouselImage/Main Banner 03" height="450px" />
							<Carousel.Caption style={containerColor}>
								<h3>Third slide label</h3>
								<p>
									{Translations.HomeCarousel}
								</p>
							</Carousel.Caption>
						</Carousel.Item>
					</Carousel>
				</div>
				<div cz-shortcut-listen="true">


					<main style={containerColor} >

						<div style={containerBg} className="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center ">
							<div className="col-md-6 p-lg-5 mx-auto my-5">
								<h1 className="display-3 fw-bold">{Translations.MainHeading}</h1>
								<h3 className="fw-normal  mb-3">{Translations.HeadingDetail}</h3>
								<div className="d-flex gap-3 justify-content-center lead fw-normal">
									<a className="icon-link" href="#">
										Learn more
										<svg className="bi"><use xlinkHref="#chevron-right"></use></svg>
									</a>
									<a className="icon-link" href="#">
										Buy
										<svg className="bi"><use xlinkHref="#chevron-right"></use></svg>
									</a>
								</div>
							</div>
							<div className={`${styles.productdevice} shadow-sm d-none d-lg-block`}></div>
							<div className={`${styles.productdevice} ${styles.productdevice2} shadow-sm d-none d-lg-block`}></div>

						</div>
						<div className="row my-md-3 ps-md-3 " >
							<div className="col-md-6">
								<div className="text-bg-dark me-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center overflow-hidden">
									<div className="my-3 py-3">
										<h2 className="display-5">{Translations.Anotherheadline}</h2>
										<p className="lead">{Translations.Anotherheadlinedetail}</p>
									</div>
									<img className="bg-body-tertiary shadow-sm mx-auto" style={{ width: "80%", height: "300px", borderRadius: "21px  21px 0 0" }}
										src="homepage/images (1).jpeg" height="300px" >
									</img>
								</div>
							</div>
							<div className="col-md-6" style={containerBg}>
								<div className=" me-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center overflow-hidden">
									<div className="my-3 p-3">
										<h2 className="display-5">{Translations.Anotherheadline}</h2>
										<p className="lead">{Translations.Anotherheadlinedetail}</p>
									</div>
									<img className="bg-dark shadow-sm mx-auto" style={{ width: "80%", height: "300px", borderRadius: "21px  21px 0 0" }}
										src="homepage/images.jpeg"  >
									</img>
								</div>
							</div>
						</div>

						<div className="row my-md-3 ps-md-3"  >
							<div className="col-md-6" >
								<div style={containerBg} className="  me-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center overflow-hidden">
									<div className="my-3 p-3">
										<h2 className="display-5">{Translations.Anotherheadline}</h2>
										<p className="lead">{Translations.Anotherheadlinedetail}</p>
									</div>
									<img className="bg-dark shadow-sm mx-auto" style={{ width: "80%", height: "300px", borderRadius: "21px  21px 0 0" }}
										src="homepage/download.jpeg" height="340px" >
									</img>
								</div>
							</div>
							<div className="col-md-6">
								<div className="text-bg-primary me-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center overflow-hidden">
									<div className="my-3 py-3">
										<h2 className="display-5">{Translations.Anotherheadline}</h2>
										<p className="lead">{Translations.Anotherheadlinedetail}</p>
									</div>
									<img className="bg-body-tertiary shadow-sm mx-auto" style={{ width: "80%", height: "300px", borderRadius: "21px  21px 0 0" }}
										src="homepage/images (4).jpeg" height="300px" >
									</img>
								</div>
							</div>
						</div>



						<div className=" my-md-3 ps-md-3 row">
							<div className="col-md-6">
								<div style={containerBg} className=" me-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center overflow-hidden">
									<div className="my-3 p-3">
										<h2 className="display-5">{Translations.Anotherheadline}</h2>
										<p className="lead">{Translations.Anotherheadlinedetail}</p>
									</div>
									<img className="bg-body shadow-sm mx-auto" style={{ width: "80%", height: "300px", borderRadius: "21px  21px 0 0" }}
										src="homepage/images (2).jpeg" height="300px" >
									</img>
								</div>
							</div>
							<div className="col-md-6">

								<div className=" bg-success me-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center overflow-hidden">
									<div className="my-3 py-3">
										<h2 className="display-5">{Translations.Anotherheadline}</h2>
										<p className="lead">{Translations.Anotherheadlinedetail}</p>
									</div>
									<img className="bg-body shadow-sm mx-auto" style={{ width: "80%", height: "300px", borderRadius: "21px  21px 0 0" }}
										src="homepage/images (3).jpeg" height="300px" >
									</img>
								</div>
							</div>
						</div>

						<div className="row my-md-3 ps-md-3 ">
							<div className="col-md-6">
								<div className=" me-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center overflow-hidden">
									<div className="my-3 p-3">
										<h2 className="display-5">{Translations.Anotherheadline}</h2>
										<p className="lead">{Translations.Anotherheadlinedetail}</p>
									</div>
									<img className="bg-body shadow-sm mx-auto" style={{ width: "80%", height: "300px", borderRadius: "21px  21px 0 0" }}
										src="homepage/download (3).jpeg" height="300px" width="420px" >
									</img>
								</div>
							</div>
							<div className="col-md-6">
								<div style={containerBg} className=" me-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center overflow-hidden">
									<div className="my-3 py-3">
										<h2 className="display-5">{Translations.Anotherheadline}</h2>
										<p className="lead">{Translations.Anotherheadlinedetail}</p>
									</div>
									<img className="bg-body shadow-sm mx-auto" style={{ width: "80%", height: "300px", borderRadius: "21px  21px 0 0" }}
										src="homepage/download (2).jpeg" height="300px" width="420px" >
									</img>
								</div>
							</div>
						</div>
					</main>

					<footer className="container py-5" style={containerColor}>
						<div className="row">
							<div className="col-12 col-md">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" className="d-block mb-2" role="img" viewBox="0 0 24 24"><title>Product</title><circle cx="12" cy="12" r="10"></circle><path d="M14.31 8l5.74 9.94M9.69 8h11.48M7.38 12l5.74-9.94M9.69 16L3.95 6.06M14.31 16H2.83m13.79-4l-5.74 9.94"></path></svg>
								<small className="d-block mb-3 ">© 2017–2023</small>
							</div>
							<div className="col-6 col-md">
								<h5>Features</h5>
								<ul className="list-unstyled text-small">
									<li><a className="link-secondary text-decoration-none" href="#">Cool stuff</a></li>
									<li><a className="link-secondary text-decoration-none" href="#">Random feature</a></li>
									<li><a className="link-secondary text-decoration-none" href="#">Team feature</a></li>
									<li><a className="link-secondary text-decoration-none" href="#">Stuff for developers</a></li>
									<li><a className="link-secondary text-decoration-none" href="#">Another one</a></li>
									<li><a className="link-secondary text-decoration-none" href="#">Last time</a></li>
								</ul>
							</div>
							<div className="col-6 col-md">
								<h5>Resources</h5>
								<ul className="list-unstyled text-small">
									<li><a className="link-secondary text-decoration-none" href="#">Resource name</a></li>
									<li><a className="link-secondary text-decoration-none" href="#">Resource</a></li>
									<li><a className="link-secondary text-decoration-none" href="#">Another resource</a></li>
									<li><a className="link-secondary text-decoration-none" href="#">Final resource</a></li>
								</ul>
							</div>
							<div className="col-6 col-md" >
								<h5>Resources</h5>
								<ul className="list-unstyled text-small">
									<li><a className="link-secondary text-decoration-none" href="#">Business</a></li>
									<li><a className="link-secondary text-decoration-none" href="#">Education</a></li>
									<li><a className="link-secondary text-decoration-none" href="#">Government</a></li>
									<li><a className="link-secondary text-decoration-none" href="#">Gaming</a></li>
								</ul>
							</div>
							<div className="col-6 col-md">
								<h5>About</h5>
								<ul className="list-unstyled text-small">
									<li><a className="link-secondary text-decoration-none" href="#">Team</a></li>
									<li><a className="link-secondary text-decoration-none" href="#">Locations</a></li>
									<li><a className="link-secondary text-decoration-none" href="#">Privacy</a></li>
									<li><a className="link-secondary text-decoration-none" href="#">Terms</a></li>
								</ul>
							</div>
						</div>
					</footer>
					<script src="/docs/5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossOrigin="anonymous"></script>
				</div>

			</div>
		</>
	)
}

export default HomePage
