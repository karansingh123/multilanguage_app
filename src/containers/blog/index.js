import languageContext from '@/components/context/language/languageContext';
import translations from '@/components/multilanguage';
import React, { useContext } from 'react'
import { useDispatch, useSelector } from 'react-redux';

const Blog = () => {
  // const { Translations, mode, toggleMode } = useContext(languageContext);
  const dispatch = useDispatch();
	const language = useSelector(state => state.language);
	const Translations = translations[language];
	const mode = useSelector(state => state.mode);
  
	const containerBg = {
	  backgroundColor: mode === "dark" ? "black" : "white",
	}
	const containerColor = { color: mode === "dark" ? "white" : "black", }	
  return (
    <>
      <div style={containerColor}>
        <div cz-shortcut-listen="true" className='' >
          <svg xmlns="http://www.w3.org/2000/svg" className="d-none">
            {/* <symbol id="aperture" fill="none" stroke="currentColor" strokeLinecap="round" strokeWinejoin="round" stroke-width="2" viewBox="0 0 24 24">
              <circle cx="12" cy="12" r="10"></circle>
              <path d="M14.31 8l5.74 9.94M9.69 8h11.48M7.38 12l5.74-9.94M9.69 16L3.95 6.06M14.31 16H2.83m13.79-4l-5.74 9.94"></path>
            </symbol> */}
            <symbol id="cart" viewBox="0 0 16 16">
              <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .49.598l-1 5a.5.5 0 0 1-.465.401l-9.397.472L4.415 11H13a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l.84 4.479 9.144-.459L13.89 4H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"></path>
            </symbol>
            <symbol id="chevron-right" viewBox="0 0 16 16">
              <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
            </symbol>
          </svg>

          <div className="container">
          </div>
          <main className="container">
            <div className="p-4 p-md-5 mb-4 mt-5 rounded text-body-emphasis " style={containerBg}>
              <div className="col-lg-6 px-0" style={containerColor}>
                <h1 className="display-4 fst-italic">{Translations.BlogTitle}</h1>
                <p className="lead my-3">{Translations.TitleContent}</p>
                <p className="lead mb-0"><a href="#" className="text-body-emphasis fw-bold">{Translations.Continuereading}</a></p>
              </div>
            </div>

            <div className="row mb-2">
              <div className="col-md-6">
                <div className="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                  <div className="col p-4 d-flex flex-column position-static" style={containerBg} >
                    <strong className="d-inline-block mb-2 text-primary">World</strong>
                    <h3 className="mb-0">Featured post</h3>
                    <div className="mb-1 text-body-secondary">Nov 12</div>
                    <p className="card-text mb-auto">{Translations.CardOne}</p>
                    <a href="#" className="icon-link gap-1 icon-link-hover stretched-link">
                    {Translations.Continuereading}
                      <svg className="bi"></svg>

                    </a>
                  </div>
                  <div className="col-auto d-none d-lg-block"  >
                    <svg className="bd-placeholder-img" width="200px" height="250px" xmlns="http://www.w3.org/2000/svg" role="img" preserveAspectRatio="xMidYMid slice" focusable="false">
                      <title>Placeholder</title>
                      <image href="thumbnailimg/download (1).jpeg" width="100%" height="100%" />
                      <text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text>
                    </svg>

                  </div>
                </div>
              </div>
              <div className="col-md-6" >
                <div style={containerBg} className="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                  <div className="col p-4 d-flex flex-column position-static">
                    <strong className="d-inline-block mb-2 text-success">Design</strong>
                    <h3 className="mb-0">Post title</h3>
                    <div className="mb-1 text-body-secondary">Nov 11</div>
                    <p className="mb-auto" >{Translations.CardTwo}</p>
                    <a href="#" className="icon-link gap-1 icon-link-hover stretched-link">
                    {Translations.Continuereading}
                      <svg className="bi"></svg>
                    </a>
                  </div>
                  <div className="col-auto d-none d-lg-block">
                    <svg className="bd-placeholder-img" width="200px" height="250px" xmlns="http://www.w3.org/2000/svg" role="img" preserveAspectRatio="xMidYMid slice" focusable="false">
                      <title>Placeholder</title>
                      <image href="thumbnailimg/images (5).jpeg" width="100%" height="100%" />
                      <text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text>
                    </svg>

                  </div>
                </div>
              </div>
            </div>

            <div className="row g-5">
              <div className="col-md-8">
                <h3 className="pb-4 mb-4 fst-italic border-bottom">
                  {Translations.FromtheFirehose}
                </h3>

                <article className="blog-post p-4" style={containerBg}>
                  <h2 className="display-5 link-body-emphasis mb-1">{Translations.Sampleblogpost}</h2>
                  <p className="blog-post-meta">January 1, 2021 by <a href="#">Mark yesss</a></p>

                  <p >{Translations.SimplePostBlog1}</p>
                  <hr />
                  <p>{Translations.SimplePostBlog2}</p>
                  <h2>Blockquotes</h2>
                  <p>{Translations.Exampleblockquote}</p>
                  <blockquote className="blockquote">
                    <p>{Translations.Quotedtext}</p>
                  </blockquote>
                  <p>{Translations.Blockquote}</p>
                  <h3>Example lists</h3>
                  <p>{Translations.BlockquoteExample}</p>

                  <h2>Inline HTML elements</h2>
                  <p>{Translations.HtmlElement}<a href="https://developer.mozilla.org/en-US/docs/Web/HTML/Element">Mozilla Developer Network</a>.</p>
                  <ul>
                    <li><strong>To bold text</strong>, use <code className="language-plaintext highlighter-rouge">&lt;strong&gt;</code>.</li>
                    <li><em>To italicize text</em>, use <code className="language-plaintext highlighter-rouge">&lt;em&gt;</code>.</li>
                    <li>Abbreviations, like <abbr title="HyperText Markup Language">HTML</abbr> should use <code className="language-plaintext highlighter-rouge">&lt;abbr&gt;</code>, with an optional <code className="language-plaintext highlighter-rouge">title</code> attribute for the full phrase.</li>
                    <li>Citations, like <cite>— Mark Otto</cite>, should use <code className="language-plaintext highlighter-rouge">&lt;cite&gt;</code>.</li>
                    <li><del>Deleted</del> text should use <code className="language-plaintext highlighter-rouge">&lt;del&gt;</code> and <ins>inserted</ins> text should use <code className="language-plaintext highlighter-rouge">&lt;ins&gt;</code>.</li>
                    <li>Superscript <sup>text</sup> uses <code className="language-plaintext highlighter-rouge">&lt;sup&gt;</code> and subscript <sub>text</sub> uses <code className="language-plaintext highlighter-rouge">&lt;sub&gt;</code>.</li>
                  </ul>
                  <p>{Translations.HtmlElementTag}</p>
                  <h2>Heading</h2>
                  <p>{Translations.BlogHeading}</p>

                </article>

                <article className="blog-post p-4 my-3" style={containerBg}>
                  <h2 className="display-5 link-body-emphasis mb-1">{Translations.Anotherblogpost2}</h2>
                  <p className="blog-post-meta">December 23, 2020 by <a href="#">Jacob</a></p>

                  <p>{Translations.BlogHeading}</p>
                  <blockquote>
                    <p>{Translations.Anotherblogpost} <strong>emphasized text</strong> {Translations.inthemiddleofit}</p>
                  </blockquote>

                </article>
                <nav className="blog-pagination" aria-label="Pagination">
                  <a className="btn btn-outline-primary rounded-pill mx-3" href="#">Older</a>
                  <a className="btn btn-outline-secondary rounded-pill disabled" aria-disabled="true">Newer</a>
                </nav>

              </div>

              <div className="col-md-4 " >
                <div className="position-sticky" style={{top:"2rem"}}>
                  <div className="p-4 mb-3  rounded" style={containerBg}>
                    <h4 className="fst-italic">About</h4>
                    <p className="mb-0">{Translations.BlogAbout}</p>
                  </div>

                  <div style={containerBg}>

                    <h4 className="fst-italic">Recent posts</h4>
                    <ul className="list-unstyled" >
                      <li>
                        <a className="d-flex flex-column flex-lg-row gap-3 align-items-start align-items-lg-center py-3 link-body-emphasis text-decoration-none border-top" href="#">
                          <svg className="bd-placeholder-img" width="100%" height="96" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false">
                            <image href="homepage/download.jpeg" width="100%" height="96" />
                          </svg>

                          <div className="col-lg-8" style={containerColor}>
                            <h6 className="mb-0">{Translations.Exampleblogpost}</h6>
                            <small className="">January 15, 2023</small>
                          </div>
                        </a>
                      </li>
                      <li>
                        <a className="d-flex flex-column flex-lg-row gap-3 align-items-start align-items-lg-center py-3 link-body-emphasis text-decoration-none border-top" href="#">
                          <svg className="bd-placeholder-img" width="100%" height="96" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false">
                            <image href="homepage/images (3).jpeg" width="100%" height="96" />
                          </svg>
                          <div className="col-lg-8" style={containerColor}>
                            <h6 className="mb-0">{Translations.AnotherBlogPostTitle}</h6>
                            <small className="">January 14, 2023</small>
                          </div>
                        </a>
                      </li>
                      <li>
                        <a className="d-flex flex-column flex-lg-row gap-3 align-items-start align-items-lg-center py-3 link-body-emphasis text-decoration-none border-top" href="#">
                          <svg className="bd-placeholder-img" width="100%" height="96" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false">
                            <image href="homepage/images (2).jpeg" width="100%" height="96" />
                          </svg>
                          <div className="col-lg-8" style={containerColor}>
                            <h6 className="mb-0">{Translations.Longerblog}</h6>
                            <small className="">January 13, 2023</small>
                          </div>
                        </a>
                      </li>
                    </ul>

                  </div>

                  <div className="p-4">
                    <h4 className="fst-italic">Archives</h4>
                    <ol className="list-unstyled mb-0">
                      <li><a href="#">March 2021</a></li>
                      <li><a href="#">February 2021</a></li>
                      <li><a href="#">January 2021</a></li>
                      <li><a href="#">December 2020</a></li>
                      <li><a href="#">November 2020</a></li>
                      <li><a href="#">October 2020</a></li>
                      <li><a href="#">September 2020</a></li>
                      <li><a href="#">August 2020</a></li>
                      <li><a href="#">July 2020</a></li>
                      <li><a href="#">June 2020</a></li>
                      <li><a href="#">May 2020</a></li>
                      <li><a href="#">April 2020</a></li>
                    </ol>
                  </div>

                  <div className="p-4">
                    <h4 className="fst-italic">Elsewhere</h4>
                    <ol className="list-unstyled">
                      <li><a href="#">GitHub</a></li>
                      <li><a href="#">Twitter</a></li>
                      <li><a href="#">Facebook</a></li>
                    </ol>
                  </div>
                </div>
              </div>
            </div>

          </main>

          <footer className="py-5 text-center mt-4 " style={containerBg}>
            <p>Blog template built for <a href="https://getbootstrap.com/">Bootstrap</a> by <a href="https://twitter.com/mdo">@mdo</a>.</p>
            <p className="mb-0">
              <a href="#">Back to top</a>
            </p>
          </footer>
          <script src="/docs/5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossOrigin="anonymous"></script>



        </div>
      </div>
    </>
  )
}

export default Blog
