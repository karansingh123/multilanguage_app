import React from 'react'
import styles from './container.module.css'
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Link from 'next/link';
import { Container, Form } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { changeLanguage, toggleMode } from '../components/redux/action/index';
import translations from '@/components/multilanguage';
// import { Cagliostro } from 'next/font/google';

const Header = () => {

  const dispatch = useDispatch();
  const language = useSelector(state => state.language);
   console.log(language, 'language')
  const Translations = translations[language];
  const mode = useSelector(state => state.mode);

  const containerBg = {
    backgroundColor: mode === "dark" ? "#212529" : "white",
  }
  const containerColor = { color: mode === "dark" ? "white" : "black", }

  const handleToggleMode = () => {
    dispatch(toggleMode());

  };

  const handleChangeLanguage = (language) => {
    dispatch(changeLanguage(language));
  };
  return (
    <div>
      <div className='mb-5'>
        <Navbar expand="lg" className=" " fixed='top' style={{ backgroundColor: mode === "dark" ? "#000" : "#fff" }}>
          <Container className='d-flex'>
            <Navbar.Brand style={containerColor} href="/">React-Bootstrap</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className=" justify-content-evenly flex-grow-1 ">
                <Link className={styles.Link} href="/">Home</Link>
                <Link className={styles.Link} href="/about">About</Link>
                <Link className={styles.Link} href="/blog">Blog</Link>
                <Link className={styles.Link} href="/content">Content</Link>
              </Nav>
              <select style={{ borderRadius: "10px" }} className=' px-3 py-2' onChange={(e) => { dispatch(changeLanguage(e.target.value)) }}>
                <option style={{ borderRadius: "10px" }} value='en'>English</option>
                <option style={{ borderRadius: "10px" }} value='fn'>French</option>
              </select>
            </Navbar.Collapse>
            <Form.Check style={containerColor}
              className=" ms-4"
              onClick={() => { dispatch(toggleMode()) }}
              type="switch"
              id="custom-switch"
              label="Dark Mode"
            />
          </Container>
        </Navbar>
      </div>

    </div>
  )
}

export default Header

