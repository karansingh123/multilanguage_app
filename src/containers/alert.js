// import React, { useEffect, useState } from 'react';
// // import styles from './container.module.css'


// function CustomAlert(props) {
//     const [show, setShow] = useState(true);

//     useEffect(() => {
//         if (props.alert) {
//             setShow(true);
//             const timer = setTimeout(() => {
//                 setShow(false);
//             }, 5000);
//             return () => clearTimeout(timer);
//         }
//     }, [props.alert]);

//     function capitalize(txt) {
//         return txt.charAt(0).toUpperCase() + txt.slice(1);
//     }
//     return (
//         <div style={{ height: '50px' }} className='mt-5'>
//             {props.alert && <div className={`alert alert-${props.alert.type} alert-dismissible fade show`} role="alert">
//                 <strong>{capitalize(props.alert.type)}</strong>: {props.alert.msg}
//             </div>}
//         </div>

//     );


// }

// export default CustomAlert; 
