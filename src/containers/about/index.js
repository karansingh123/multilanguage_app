import languageContext from '@/components/context/language/languageContext';
import translations from '@/components/multilanguage';
import React, { useContext } from 'react'
import Accordion from 'react-bootstrap/Accordion';
import { useDispatch, useSelector } from 'react-redux';

const About = () => {
  // const { Translations, mode } = useContext(languageContext);
  const dispatch = useDispatch();
	const language = useSelector(state => state.language);
	const Translations = translations[language];
	const mode = useSelector(state => state.mode);
  
	const containerBg = {
	  backgroundColor: mode === "dark" ? "black" : "white",
	}
	const containerColor = { color: mode === "dark" ? "white" : "black", }	
  return (
    <>
      <div className='' style={containerColor}>
        <Accordion defaultActiveKey="0">
          <Accordion.Item eventKey="0">
            <Accordion.Header >Accordion Item #1</Accordion.Header>
            <Accordion.Body style={containerBg}>
              <span style={containerColor}>
                {Translations.AccordionItem1}
              </span>
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="1">
            <Accordion.Header>Accordion Item #2</Accordion.Header>
            <Accordion.Body style={containerBg}>
              <span style={containerColor}>
                {Translations.AccordionItem2}
              </span>
            </Accordion.Body>
          </Accordion.Item>
        </Accordion>
      </div>
      <div cz-shortcut-listen="true" style={containerColor}>
        <main >
          <div className="b-example-divider"></div>

          <div className="container px-4 py-5" id="hanging-icons" style={containerBg}>
            <h2 className="pb-2 border-bottom">Hanging icons</h2>
            <div className="row g-4 py-5 row-cols-1 row-cols-lg-3">
              <div className="col d-flex align-items-start">
                <div className="icon-square  bg-body-secondary d-inline-flex align-items-center justify-content-center fs-4 flex-shrink-0 me-3">

                </div>
                <div>
                  <h3 className="fs-2 ">{Translations.Featuredtitle}</h3>
                  <p>{Translations.TitleParagrapg}</p>
                  <a href="#" className="btn btn-primary">
                  {Translations.Primarybutton}
                  </a>
                </div>
              </div>
              <div className="col d-flex align-items-start">
                <div className="icon-square  bg-body-secondary d-inline-flex align-items-center justify-content-center fs-4 flex-shrink-0 me-3">

                </div>
                <div>
                  <h3 className="fs-2 ">{Translations.Featuredtitle}</h3>
                  <p>{Translations.TitleParagrapg}</p>
                  <a href="#" className="btn btn-primary">
                  {Translations.Primarybutton}
                  </a>
                </div>
              </div>
              <div className="col d-flex align-items-start">
                <div className="icon-square  bg-body-secondary d-inline-flex align-items-center justify-content-center fs-4 flex-shrink-0 me-3">
                </div>
                <div>
                  <h3 className="fs-2 ">{Translations.Featuredtitle}</h3>
                  <p>{Translations.TitleParagrapg}</p>
                  <a href="#" className="btn btn-primary">
                  {Translations.Primarybutton}
                  </a>
                </div>
              </div>
            </div>
          </div>

          <div className="b-example-divider"></div>

          <div className="container px-4 py-5" id="custom-cards">
            <h2 className="pb-2 border-bottom">Custom cards</h2>

            <div className="row row-cols-1 row-cols-lg-3 align-items-stretch g-4 py-5" style={containerBg}>
              <div className="col">
                <div className="card card-cover h-100 overflow-hidden text-bg-dark rounded-4 shadow-lg" style={{ backgroundImage: "url('homepage/images (1).png')" }}>
                  <div className="d-flex flex-column h-100 p-5 pb-3 text-white text-shadow-1">
                    <h3 className="pt-5 mt-5 mb-4 display-6 lh-1 fw-bold">{Translations.ImgText1}</h3>
                    <ul className="d-flex list-unstyled mt-auto">
                      <li className="me-auto">
                        <img src="homepage/pexels-sebastiaan-stam-1097456.jpg" alt="Bootstrap" width="32" height="32" className="rounded-circle border border-white" />
                      </li>
                      <li className="d-flex align-items-center me-3">
                        <svg className="bi me-2" width="1em" height="1em"><use xliinkherf="#geo-fill"></use></svg>
                        <small>Earth</small>
                      </li>
                      <li className="d-flex align-items-center">
                        <svg className="bi me-2" width="1em" height="1em"><use xliinkherf="#calendar3"></use></svg>
                        <small>3d</small>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>

              <div className="col">
                <div className="card card-cover h-100 overflow-hidden text-bg-dark rounded-4 shadow-lg" style={{ backgroundImage: "url('homepage/pexels-sebastiaan-stam-1097456.jpg')" }}>
                  <div className="d-flex flex-column h-100 p-5 pb-3 text-white text-shadow-1">
                    <h3 className="pt-5 mt-5 mb-4 display-6 lh-1 fw-bold">{Translations.ImgText2}</h3>
                    <ul className="d-flex list-unstyled mt-auto">
                      <li className="me-auto">
                        <img src="homepage/pexels-sebastiaan-stam-1097456.jpg" alt="Bootstrap" width="32" height="32" className="rounded-circle border border-white" />
                      </li>
                      <li className="d-flex align-items-center me-3">
                        <svg className="bi me-2" width="1em" height="1em"><use xliinkherf="#geo-fill"></use></svg>
                        <small>Pakistan</small>
                      </li>
                      <li className="d-flex align-items-center">
                        <svg className="bi me-2" width="1em" height="1em"><use xliinkherf="#calendar3"></use></svg>
                        <small>4d</small>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>

              <div className="col">
                <div className="card card-cover h-100 overflow-hidden text-bg-dark rounded-4 shadow-lg" style={{ backgroundImage: "url('homepage/download.png')" }}>
                  <div className="d-flex flex-column h-100 p-5 pb-3 text-shadow-1">
                    <h3 className="pt-5 mt-5 mb-4 display-6 lh-1 fw-bold">{Translations.ImgText3}</h3>
                    <ul className="d-flex list-unstyled mt-auto">
                      <li className="me-auto">
                        <img src="homepage/pexels-sebastiaan-stam-1097456.jpg" alt="Bootstrap" width="32" height="32" className="rounded-circle border border-white" />
                      </li>
                      <li className="d-flex align-items-center me-3">
                        <svg className="bi me-2" width="1em" height="1em"><use xliinkherf="#geo-fill"></use></svg>
                        <small>California</small>
                      </li>
                      <li className="d-flex align-items-center">
                        <svg className="bi me-2" width="1em" height="1em"><use xliinkherf="#calendar3"></use></svg>
                        <small>5d</small>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="b-example-divider"></div>


          <div className="b-example-divider"></div>

          <div className="container px-4 py-5" style={containerBg}>
            <h2 className="pb-2 border-bottom">{Translations.Featureswithtitle}</h2>

            <div className="row row-cols-1 row-cols-md-2 align-items-md-center g-5 py-5" >
              <div className="col d-flex flex-column align-items-start gap-2">
                <h2 className="fw-bold ">{Translations.Featurespage}</h2>
                <p className="">{Translations.FeaturespageParagraph}</p>
                <a href="#" className="btn btn-primary btn-lg">{Translations.Primarybutton}</a>
              </div>

              <div className="col">
                <div className="row row-cols-1 row-cols-sm-2 g-4">
                  <div className="col d-flex flex-column gap-2">
                    <img className="feature-icon-small d-inline-flex align-items-center justify-content-center text-bg-primary bg-gradient fs-4 rounded-3  p-2"
                      width="40px" height="40px"
                      src="settings_wheel_icon">

                    </img>
                    <h4 className="fw-semibold mb-0 ">{Translations.Featuredtitle}</h4>
                    <p className="">{Translations.TitleParagrapg}</p>
                  </div>

                  <div className="col d-flex flex-column gap-2">
                    <img className="feature-icon-small d-inline-flex align-items-center justify-content-center text-bg-primary bg-gradient fs-4 rounded-3  p-2"
                      width="40px" height="40px"
                      src="tools.png">

                    </img>
                    <h4 className="fw-semibold mb-0 ">{Translations.Featuredtitle}</h4>
                    <p className="">{Translations.TitleParagrapg}</p>
                  </div>

                  <div className="col d-flex flex-column gap-2">
                    <img className="feature-icon-small d-inline-flex align-items-center justify-content-center text-bg-primary bg-gradient fs-4 rounded-3  p-2"
                      width="40px" height="40px"
                      src="microchip.png">

                    </img>
                    <h4 className="fw-semibold mb-0 ">{Translations.Featuredtitle}</h4>
                    <p className="">{Translations.TitleParagrapg}</p>
                  </div>

                  <div className="col d-flex flex-column gap-2">
                    <img className="feature-icon-small d-inline-flex align-items-center justify-content-center text-bg-primary bg-gradient fs-4 rounded-3 p-2 "
                      width="40px" height="40px"
                      src="switch.png">

                    </img>
                    <h4 className="fw-semibold mb-0 ">{Translations.Featuredtitle}</h4>
                    <p className="">{Translations.TitleParagrapg}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main> <div className="col d-flex flex-column gap-2">

        </div>
        <script src="/docs/5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossOrigin="anonymous"></script>
      </div>

    </>

  )
}

export default About
